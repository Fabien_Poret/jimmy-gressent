import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as firebase from 'firebase';
import { UserService } from 'src/app/services/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isConnected = false;
  uid = '';
  adminCheck = false;

  constructor(
    private authentification: AuthenticationService,
    private router: Router,
    private toastr: ToastrService,
    private usersService: UserService
  ) { }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged(
      (user) => {
      if (user) {
        this.isConnected = true;
        this.uid = user.uid;
        console.log('Connected :' + user);
        this.usersService.roleVerified({roles : [ 'admin']}).then((value) => {
          if (value == true) {
            this.adminCheck = true;
            console.log(this.adminCheck);
          }
        })
      } else {
        this.isConnected = false;
      }
    })
  
  }

  onSignOut(){
    this.authentification.logout().then(
      (data) => {
        console.log('Déconnexion');
        this.adminCheck = false;
        this.router.navigate(['/']);
      })
      .catch(
        (error) => {
          console.log(error);
        }
    )
  }

  
}
