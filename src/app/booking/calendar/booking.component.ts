import { Component, OnInit, ViewChild  } from '@angular/core';
import { UserService } from '../../services/users.service';
import { CalendarOptions, DateSelectArg, EventClickArg, EventApi, Calendar, CalendarApi } from '@fullcalendar/angular'; // useful for typechecking
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { EventSService } from 'src/app/services/events.service';
import { Event } from 'src/app/interface/event';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { User } from 'src/app/interface/user';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  constructor(
    private router : Router,
    private usersService: UserService,
    private formBuilder: FormBuilder,
    private eventsService: EventSService,
    private toastr: ToastrService
  ) { }
  
  // TODO: Rendre cliquable les évents 
  // TODO: Créer une modal pour voir les clients inscrits sur un cours et dans cette modal proposer l'édition 
  // TODO: Créer une désinscription qui envoie des mails et SMS aux participants 
  // TODO : Gérer l'affichage d'un utilisateur par son UID même si il est supprimé 
  
  eventsSubscription: Subscription;
  eventsForm: FormGroup;
  suppressionForm: FormGroup;
  events: any[] = [];
  currentEvents: EventApi[] = [];
  calendarOptions: CalendarOptions;
  adminCheck = false;
  eventShow: Event;
  uid;
  registrationAtThisEvent = false;
  userConnected: User;
  editMode = false;
  indexToUpdate;
  indexEventShow;
  countUsers;

  ngOnInit(): void {
    this.initEventsForm();
    this.initSuppressionForm();

    this.eventsSubscription = this.eventsService.eventsSubject.subscribe(
      (data: Event[]) => {
        this.events = data;
        this.calendarOptions = {
          initialView: 'dayGridMonth',
          editable: false,
          selectable: true,
          headerToolbar: {
          },
          events: data,
          eventClick: this.eventClick.bind(this),
          dateClick: this.handleDateClick.bind(this), // bind is important!
          locale: 'fr',
          timeZone: 'UTC',
          eventsSet: this.handleEvents.bind(this)
        }
        console.log(this.events);
      }
    );
    this.eventsService.getEvents();
    this.eventsService.emitEvents();
    console.log(this.events);
    this.uid = this.usersService.getUserConnected();
    this.usersService.getUserByUid(this.uid).then(
      (value: User) => {
        this.userConnected = value;
        console.log(this.userConnected);
      })
    this.usersService.roleVerified({roles : [ 'admin']}).then((value) => {
      if (value == true) {
        this.adminCheck = true;
      }
    })
  }

 

  initEventsForm(){
    this.eventsForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(4)]],
      start:[new Date(), Validators.required ],
      duration : ['', Validators.required ],
      end: [new Date() ],
      backgroundColor: ['#ff0000'],
      description: '',
      id: '',
      users: ['']
    })
  }

  initSuppressionForm(){
    this.suppressionForm = this.formBuilder.group({
      email: [true],
      sms: [false],
      push: [false]
    })
  }

  onSubmitEventsForm(){
    console.log("Oui");
    const newEvent = this.eventsForm.value;

    // TODO: Code à améliorer / rendre plus lisible 
    const newDate = new Date(newEvent.start);
    const hours = newDate.getHours() + newEvent.duration;
    newDate.setHours(hours);
    const dateFormat = new Intl.DateTimeFormat('fr', {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    }).format(newDate);
    const removeSpace = dateFormat.split(' ');
    const hoursSplit = removeSpace[2] + ':00';
    const dateEndSplit = removeSpace[0].split('/');
    const newDateEndWithSplit = dateEndSplit[2] + '-' + dateEndSplit[1] + '-' + dateEndSplit[0] + ' ' + hoursSplit;
    console.log(newDateEndWithSplit);
    newEvent.end = newDateEndWithSplit;

    const dateSplit = newEvent.start.split('T');
    console.log(dateSplit);
    newEvent.start = dateSplit[0] + ' ' + dateSplit[1] + ':00';

    if (newEvent.title == "Libre") {
      newEvent.backgroundColor = '#e70000';
    } else if (newEvent.title == "Renforcement") {
      newEvent.backgroundColor = '#ff8c00';
    } else if (newEvent.title == "Théorie techniques d'apprentissage") {
      newEvent.backgroundColor = '#ffef00';
    } else if (newEvent.title == "Mise en pratique") {
      newEvent.backgroundColor = '#00811f';
    } else if (newEvent.title == "Refus d'appât") {
      newEvent.backgroundColor = '#0044ff';
    } else if (newEvent.title == "Formation théorique sur développement relationnel et éducatif au sein du foyer") {
      newEvent.backgroundColor = '#5762a3';
    } else if (newEvent.title == "Divers") {
      newEvent.backgroundColor = '#5e230f';
    }
    newEvent.id = Math.random().toString(36).substr(2, 9);

    if (!this.editMode) {
      console.log(newEvent);
      this.eventsService.createEvent(newEvent);
    } else{
      this.eventsService.updateProperty(newEvent, this.indexToUpdate);
      this.editMode = false;
    }

    $('#eventModalCenter').modal('hide');
  }

  resetForm() {
    this.eventsForm.reset();
  }

  eventClick(event) {
    this.resetForm();
    $('#eventModal').modal('show');
    
    this.eventsService.getEventByInternalId(event.event._def.publicId).then(
      (event: Event) => {
        this.eventShow = event;
        this.countUsers = event.users.length;
        if(event.users && event.users.find(user => user.uid == this.uid)){
          this.registrationAtThisEvent = true;
          console.log("inscrit");
        }else{
          this.registrationAtThisEvent = false;
          console.log("Pas inscrit");
        };
      }
    ).catch(
      (error) => {
        console.log(error);
      }
    );

    this.indexEventShow = this.events.findIndex(
      (element) => {
        if (element.id == this.eventShow.id) {
          return true;
        }
      }
    )
  }

  eventEdit(){
    $('#eventModal').modal('hide');
    $('#eventModalCenter').modal('show');

    const newDate = this.eventShow.start.split(' ')
    const hours = newDate[1].split(':');
    const start = newDate[0] + 'T' + hours[0] + ':' + hours[1];
    console.log(start);
    this.editMode = true;
    this.eventsForm.get('title').setValue(this.eventShow.title);
    this.eventsForm.get('start').setValue(start);
    this.eventsForm.get('duration').setValue(this.eventShow.duration);

    if (this.eventShow.description) {
      this.eventsForm.get('description').setValue(this.eventShow.description); 
    }
    if (this.eventShow.users) {
      this.eventsForm.get('users').setValue(this.eventShow.users); 
    }

    const index = this.events.findIndex(
      (element) => {
        if (element.id == this.eventShow.id) {
          return true;
        }
      }
    )

    this.indexToUpdate = index;

  }

  handleDateClick(arg) {
    this.resetForm();

    this.usersService.roleVerified({roles : [ 'admin']}).then((value) => {
      if (value == true) {
        this.resetForm();
        this.eventsForm.get('start').setValue(arg.dateStr+'T18:00');
        $('#eventModalCenter').modal('show');
      }
    })
  }

  handleEvents(events: EventApi[]){
    this.currentEvents = events;
  }

  eventRegistration(id){
    console.log(id);
    this.eventsService.getEventByInternalId(id).then(
      (event: Event) => {
        console.log(event);
        const index = this.events.findIndex(
          (element) => {
            if (element.id == event.id) {
              return true;
            }
          }
        )
        let users = [];
        if (event.users) {
          users = event.users;
          console.log(users);
          if(event.users.find(user => user.uid == this.uid)){
            this.toastr.info('Vous êtes déjà inscrit à ce cours');
          }else{
            users.push({
                  uid: this.uid,
                  firstName: this.userConnected.firstName,
                  lastName: this.userConnected.lastName
                });
                event.users = users; 
                this.eventsService.updateProperty(event, index);
          };
        }else{
          users.push({
            uid: this.uid,
            firstName: this.userConnected.firstName,
            lastName: this.userConnected.lastName
          });
          event.users = users; 
          this.eventsService.updateProperty(event, index);
        }
      }
    ).catch(
      (error) => {
        console.log(error);
      }
    );
  }

  eventDeregistration(eventId){
    console.log(eventId);

    const index = this.events.findIndex(
      (element) => {
        if (element.id == this.eventShow.id) {
          return true;
        }
      }
    )

    console.log(this.eventShow);  
    console.log(this.eventShow.users.find(user => user.uid == this.uid));

    let users = [];
    users = this.eventShow.users;
    const position = users.indexOf(this.eventShow.users.find(user => user.uid == this.uid));
    console.log(position);  

    users.splice(position, 1);
    this.eventShow.users = users;
    console.log(this.eventShow);
    this.eventsService.updateProperty(this.eventShow, index);
  }

  onConfirmEventDelete(){
    $('#eventModal').modal('hide');
    $('#eventDeleteModal').modal('show');
  }

  eventDelete(){
    $('#eventDeleteModal').modal('hide');

    const notification = this.suppressionForm.value;
    console.log(notification);

    console.log(this.eventShow);
    const index = this.events.findIndex(
      (element) => {
        if (element.id == this.eventShow.id) {
          return true;
        }
      }
    )
    console.log(index);
    this.eventShow.id = '';
    this.eventShow.start = '';
    this.eventShow.backgroundColor = '';
    this.eventShow.duration = 0;
    this.eventShow.end = '';
    this.eventShow.title = '';
    this.eventShow.users = [''];
    this.eventsService.updateProperty(this.eventShow, index);
  }
}
