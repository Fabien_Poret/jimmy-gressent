import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Event } from '../interface/event';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class EventSService {

  constructor(private toastr: ToastrService) { }

  events: Event[] = [];
  eventsSubject = new Subject<Event[]>();

  emitEvents(){
    this.eventsSubject.next(this.events);
  }

  saveEvents(){
    firebase.database().ref('/events').set(this.events);
  }

  getEvents(){
    firebase.database().ref('/events').on('value', 
      (data) => {
        this.events = data.val() ? data.val() : [];
        this.emitEvents();
      }
    )
  }

  getEventByInternalId(id){
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/events/').orderByChild("id").equalTo(id).on("child_added", 
        function(snapshot) {
            resolve(snapshot.val());
        });
    })
  }

  getSingleEvent(id){
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/events/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          }
        ).catch(
          (error) => {
            reject(error);
          }
        )
      }
    )
  }
  
  createEvent(event: Event){
    this.events.push(event);
    this.saveEvents();
    this.emitEvents();
    this.toastr.success('L\'événement a bien été ajouté');
  }

  updateProperty(event : Event, index){
    // La property ayant l'index X est = à la nouvelle property
    this.events[index] = event;
    // Je sauvegarde les données de mon property dans la base de donnée firebase 
    this.saveEvents();
    // Je met à jour mon écouteur
    this.emitEvents();
    this.toastr.info('L\'événement est modifié', event.title);
  }


  // TODO : Get all 
  // TODO : Get one 
  // TODO : set one 
  // TODO : update 
}
