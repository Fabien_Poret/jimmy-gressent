import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import * as firebase from 'firebase';
import { UserService } from './users.service';
import { User } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(
    private router: Router,
    private usersService: UserService
  ) { }

  user: User;

  // Si l'utilisateur n'est pas connecté, ça redirige vers /home 
  canActivate(next: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean{
    return new Promise(
      (resolve, reject) => {
        firebase.auth().onAuthStateChanged(
          (userSession) => {
            // Si l'utilisateur est connecté alors
            if (userSession) {
              console.log(userSession);
              const user = this.usersService.getUserByUid(userSession.uid).then(
                (user: User) => {
                  this.user = user;
                  const roles = next.data['roles'];
                  let roleCheck = false;
                  for (const role of user.roles){
                    if (roles.includes(role)) {
                      roleCheck = true;
                      resolve(true);
                    }
                  }
                  if (roleCheck == false) {
                    this.router.navigate(['/error403']);
                    resolve(false);
                  }
                }
              ).catch(
                (error) => {
                  console.error(error);
                }
              );
              console.log(user);
              // Chercher dans la base de donnée de l'utilisateur avec son uid si il y a bien la variable rôle qui lui est attitré
              // Resolve true 
            } else {
              this.router.navigate(['/login']);
              resolve(false);
            }
          }
        )
    })
  }
  
  // // Si l'utilisateur n'est pas connecté, ça redirige vers /home 
  // canActivate(): Observable<boolean> | Promise<boolean> | boolean{
  //   return new Promise(
  //     (resolve, reject) => {
  //       firebase.auth().onAuthStateChanged(
  //         (userSession) => {
  //           if (userSession) {
  //             resolve(true);
  //           } else {
  //             this.router.navigate(['/login']);
  //             resolve(false);
  //           }
  //         }
  //       )
  //     }
  //   )
  // }

}
