import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { ToastrService } from 'ngx-toastr';
import { User } from '../interface/user';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private toastr: ToastrService
  ) {}

  users: User[] = [];

  usersSubject = new Subject<User[]>();

  emitUsers(){
    console.log(this.usersSubject);
    this.usersSubject.next(this.users);
    console.log('Ecouteur mise à jour');
  }

  saveUsersInFirebase(){
    firebase.database().ref('/users/').set(this.users);
    console.log('Ajout à la base de donnée');
  }

  getUsers() {
    firebase.database().ref('/users/').on('value',
      (data) => {
        console.log(data.val());
        this.users = data.val() ? data.val() : [];
        this.emitUsers();
      }
    )
  }

  getUserByUid(uid){
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/users/').orderByChild("uid").equalTo(uid).on("child_added", 
        function(snapshot) {
          resolve(snapshot.val());
        });
    })
  }

  getUserByValidation(boolean){
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/users/').orderByChild("validation").equalTo(boolean).on("child_added", 
        function(snapshot) {
          console.log(snapshot.val());
          resolve(console.log("yes"));
        });
    })
  }

  getUserConnected(){
    var user = firebase.auth().currentUser;
    console.log(user);
    return user['uid'];
  }

  createUser(user : User){
    console.log(this.users);
    console.log(user);
    this.users.push(user);
    this.saveUsersInFirebase();
    this.emitUsers();
    this.toastr.success(user.firstName + user.lastName, 'A bien été ajouté.');
    console.log(this.users);

  }

  updateUser(user : User, index){
    console.log(this.users);
    // La property ayant l'index X est = à la nouvelle property
    this.users[index] = user;
    // Je sauvegarde les données de mon property dans la base de donnée firebase 
    this.saveUsersInFirebase();
    // Je met à jour mon écouteur
    this.emitUsers();
    this.toastr.info('Le client a bien été modifié', user.firstName + ' ' + user.lastName);
    console.log(this.users);

  }


  // roleVerified(role){
  //   const uid = this.getUserConnected();
  //   const user = this.getUserByUid(uid).then(
  //     (user: User) => {
  //       const roles = role['roles'];
  //       let roleCheck = false;
  //       for (const role of user.roles){
  //         if (roles.includes(role)) {
  //           console.log('Role ok');
  //           roleCheck = true;
  //           return true;
  //         }
  //       }
  //       if (roleCheck == false) {
  //         console.log('Role pas ok');
  //         return false;
  //       }
  //     }
  //   )
  // }

  roleVerified(role){
    return new Promise(
      (resolve, reject) => {
        const uid = this.getUserConnected();
        const user = this.getUserByUid(uid).then(
          (user: User) => {
            const roles = role['roles'];
            let roleCheck = false;
            for (const role of user.roles){
              if (roles.includes(role)) {
                console.log('Role ok');
                roleCheck = true;
                // return true;
                resolve(true);
              }
            }
            if (roleCheck == false) {
              console.log('Role pas ok');
              // return false;
              resolve(false);
            }
          }
        )
      }
    )
  }

  deleteUser(index){
    // J'enleve la property de index X 
    this.users.splice(index, 1);
    // Je sauvegarde les données de mon property dans la base de donnée firebase 
    this.saveUsersInFirebase();
    // Je met à jour mon écouteur
    this.emitUsers();
    this.toastr.warning('L\'utilisateur est supprimé');
  }



  uploadFile(file: File){
    // Promise = traitement de façon asynchrone 
    return new Promise(
      (resolve, reject) => {
        // Je génère un nom de fichier unique 
        const uniqueId = Date.now().toString();
        const fileName = uniqueId + file.name;
        const upload = firebase.storage().ref().child('images/users/' + fileName).put(file);

        upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
          // Pending 
          () => {
            console.log('Chargement ...');
          },
          // Error
          (error) => {
            console.log(error);
            reject(error);
          },
          // Success
          () => {
            upload.snapshot.ref.getDownloadURL().then(
              (downLoadUrl) => {
                resolve(downLoadUrl);
              }
            )
          }
        );
      }
    );
  }

  removeFile(fileLink: string){
    if(fileLink){
      return new Promise(
        (resolve, reject) => {
          const storageRef = firebase.storage().refFromURL(fileLink);
          storageRef.delete().then(
            () => {
              console.log('File deleted');
              resolve();
            }
          ).catch(
            (error) => {
              console.error(error);
            }
          );
        }
      )
    }  
  }

  // TODO : Count 
  countValidation(){

  }

  countContractEndSoon(){

  }
}


