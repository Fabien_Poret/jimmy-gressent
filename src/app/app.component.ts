import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(){

    // TODO: Ajouter une config Firebase pour gérer la BDD.
    const firebaseConfig = {

    };
    firebase.initializeApp(firebaseConfig);
  }



}
