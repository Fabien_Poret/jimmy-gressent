import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/users.service';
import { User } from 'src/app/interface/user';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {


  constructor(
    private usersService: UserService,
    private formBuilder: FormBuilder
    ) { }

  usersSubscription: Subscription;
  users: any[] = [];
  elementAtValidation = false;
  userToRemove : User;
  indexToRemove;

  userForm: FormGroup;
  userToEdit : User;
  indexToEdit;

  ngOnInit(): void {
    this.usersSubscription = this.usersService.usersSubject.subscribe(
      (data: User[]) => {
        this.users = data;
        console.log(data);

        data.forEach((element) => {
          if (element.validation == true) {
          this.elementAtValidation = true;

          }
        })
      }
    );
    this.usersService.getUsers();
    this.usersService.emitUsers();
    console.log(this.users);
    this.initUserForm();  
    this.resetForm();
  }

  initUserForm(){
    this.userForm = this.formBuilder.group({
      // uid: '',
      email: ['', [Validators.required, Validators.email]],
      // password: ['', [Validators.required, Validators.minLength(6)]],
      firstName: ['', Validators.required ],
      lastName: ['', Validators.required ],
      phone: ['', Validators.required ],
      adress: ['', Validators.required ],
      postalCode: ['', Validators.required ],
      city: ['', Validators.required ],
      // roles: [],
      // validation: '',
      start: ['', Validators.required ],
      end: ['', Validators.required ]
     });
  }

  resetForm(){
    this.userForm.reset();
  }

  openDeleteModal(user, index){
    $('#userDeleteModal').modal('show');
    this.userToRemove = user;
    this.indexToRemove = index;
  }

  deleteUser(){
    console.log(this.userToRemove);
    console.log(this.indexToRemove);
    this.usersService.deleteUser(this.indexToRemove);
    $('#userDeleteModal').modal('hide');
  }

  filter(){
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter2
  }

  onValidation(user, index){
    console.log(user);
    console.log(index);
    user.validation = true;
    user.roles = ['user', 'customer'];
    this.usersService.updateUser(user, index);
    this.users.splice(index, 1);
    if (this.users.length <= 0 ) {
      this.elementAtValidation = false;
    }
  }

  openEditModal(user, index){
    console.log(user);
    this.indexToEdit = index;
    this.userToEdit = user;
    console.log(index);
    this.userForm.get('email').setValue(user.email);
    this.userForm.get('firstName').setValue(user.firstName);
    this.userForm.get('lastName').setValue(user.lastName);
    this.userForm.get('adress').setValue(user.adress);
    this.userForm.get('postalCode').setValue(user.postalCode);
    this.userForm.get('city').setValue(user.city);
    this.userForm.get('phone').setValue(user.phone);

    this.userForm.get('start').setValue(user.contracts[0].start);
    this.userForm.get('end').setValue(user.contracts[0].end);

    $('#userModalCenter').modal('show');
  }

  editUser(){
    $('#userModalCenter').modal('hide');
    const userFormValue = this.userForm.value;
    this.userToEdit.email = userFormValue.email;
    this.userToEdit.firstName = userFormValue.firstName;
    this.userToEdit.lastName = userFormValue.lastName;
    this.userToEdit.postalCode = userFormValue.postalCode;
    this.userToEdit.city = userFormValue.city;
    this.userToEdit.phone = userFormValue.phone;
    this.userToEdit.contracts[0].start = userFormValue.start;
    this.userToEdit.contracts[0].end = userFormValue.end;
    console.log(this.userToEdit);
    this.usersService.updateUser(this.userToEdit, this.indexToEdit);
    this.resetForm();
  }
}