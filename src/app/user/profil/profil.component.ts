import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/users.service';
import { User } from 'src/app/interface/user';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private usersService: UserService
  ) { }
  
  user;
  chargement = true;

  ngOnInit(): void {
    // TODO : Vider la variable user à l'initiation pour éviter les bugs en passant d'un profil à l'autre

    const uid = this.route.snapshot.paramMap.get('id');
    this.usersService.getUserByUid(uid).then(
      (user: User) => {
        this.user = user;
        this.chargement = false;
      }
    ).catch(
      (error) => {
        console.error(error);
      }
    );
  }

}
