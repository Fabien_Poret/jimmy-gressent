import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';
import { UserService } from '../services/users.service';
import { AuthGuardService } from '../services/auth-gard.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  

  constructor(
    private userService: UserService,
  ) { }



  ngOnInit() {
  }

  scroll(){
    $('.scroll-down').click (function() {
      $('html, body').animate({scrollTop: $('div.scrollHere').offset().top }, 'slow');
      return false;
    });
  }
}
