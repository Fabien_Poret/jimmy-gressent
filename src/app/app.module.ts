import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './_parts/header/header.component';
import { FooterComponent } from './_parts/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule  } from "@angular/forms";
import { RegistrationComponent } from './authentication/registration/registration.component';
import { LoginComponent } from './authentication/login/login.component';
import { BookingComponent } from './booking/calendar/booking.component';
import { VideoComponent } from './video/video.component';
import { ToastrModule } from 'ngx-toastr';
import { ProfilComponent } from './user/profil/profil.component';
import { UserComponent } from './user/user/user.component';
import { Error403Component } from './error/error403/error403.component';
import { ContactComponent } from './contact/contact.component';
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import { ProgrammeComponent } from './programme/programme.component';
import { ValidationComponent } from './user/validation/validation.component';
import { ContractComponent } from './user/contract/contract.component';
import { TarificationComponent } from './tarification/tarification.component';
import { MentionComponent } from './legalContent/mention/mention.component';
import { RgpdComponent } from './legalContent/rgpd/rgpd.component';
import { SitemapComponent } from './sitemap/sitemap.component'; // a plugin

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  timeGridPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    RegistrationComponent,
    LoginComponent,
    BookingComponent,
    VideoComponent,
    ProfilComponent,
    UserComponent,
    Error403Component,
    ContactComponent,
    ProgrammeComponent,
    ValidationComponent,
    ContractComponent,
    TarificationComponent,
    MentionComponent,
    RgpdComponent,
    SitemapComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-left',
      preventDuplicates: true,
      progressBar: true,
      closeButton: true,
    }), // ToastrModule added
    FullCalendarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
