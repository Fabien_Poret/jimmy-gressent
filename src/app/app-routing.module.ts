import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './authentication/login/login.component';
import { RegistrationComponent } from './authentication/registration/registration.component';
import { BookingComponent } from './booking/calendar/booking.component';
import { VideoComponent } from './video/video.component';
import { ProfilComponent } from './user/profil/profil.component';
import { UserComponent } from './user/user/user.component';
import { AuthGuardService } from './services/auth-gard.service';
import { Error403Component } from './error/error403/error403.component';
import { ContactComponent } from './contact/contact.component';
import { ProgrammeComponent } from './programme/programme.component';
import { ValidationComponent } from './user/validation/validation.component';
import { ContractComponent } from './user/contract/contract.component';
import { TarificationComponent } from './tarification/tarification.component';
import { SitemapComponent } from './sitemap/sitemap.component';
import { MentionComponent } from './legalContent/mention/mention.component';
import { RgpdComponent } from './legalContent/rgpd/rgpd.component';


const routes: Routes = [
  {
    path : '',
    component: HomeComponent,
  },
  {
    path : 'programme',
    component: ProgrammeComponent,
  },
  {
    path : 'tarification',
    component: TarificationComponent,
  },
  {
    path : 'sitemap',
    component: SitemapComponent,
  },
  {
    path : 'contract',
    component: ContractComponent,
  },
  {
    path : 'mention',
    component: MentionComponent,
  },
  {
    path : 'rgpd',
    component: RgpdComponent,
  },
  {
    path : 'login', 
    component :  LoginComponent
  },
  {
    path : 'registration', 
    component :  RegistrationComponent
  },
  {
    path: 'booking',
    canActivate: [AuthGuardService],
    component: BookingComponent,
    data : {
      roles: ['customer', 'admin']
    }
  },
  {
    path: 'video',
    canActivate: [AuthGuardService],
    component: VideoComponent,
    data : {
      roles: ['customer', 'admin']
    }
  },
  {
    path: 'contact',
    component: ContactComponent,
  },
  {
    path: 'profil/:id',
    canActivate: [AuthGuardService],
    component: ProfilComponent,
    data : {
      roles: ['user', 'customer', 'admin']
    }
  },
  {
    path: 'user',
    canActivate: [AuthGuardService],
    component: UserComponent,
    data : {
      roles: ['admin']
    }
  },
  {
    path: 'validation',
    canActivate: [AuthGuardService],
    component: ValidationComponent,
    data : {
      roles: ['admin']
    }
  },  
  {
    path: 'contract',
    canActivate: [AuthGuardService],
    component: ContractComponent,
    data : {
      roles: ['admin']
    }
  },  
  {
    path: 'error403',
    component: Error403Component,
  },
  {
    path: '*',
    redirectTo: '' 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
