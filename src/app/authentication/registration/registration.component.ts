import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { User } from 'src/app/interface/user';
import { UserService } from 'src/app/services/users.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

 // Je type mon loginForm 
 registrationForm : FormGroup;
 usersSubscription: Subscription;
 users: any[] = [];
 errorForm: '';

 photoUploading = false; 
 photoUploaded = false;
 photosAdded: any[] = [];


 // Je récupère mon formBuilder et mon service auth 
 constructor(
   private formBuilder: FormBuilder,
   private authentication: AuthenticationService,
   private router: Router,
   private usersService: UserService
   ) {
 }

 ngOnInit(): void {

  this.initRegistrationForm();
  this.registrationForm.reset();
  this.usersSubscription = this.usersService.usersSubject.subscribe(
    (data: User[]) => {
      this.users = data;
    }
  );
  this.usersService.getUsers();
  this.usersService.emitUsers();
 }

 initRegistrationForm(){
   // Permet de créer un JSON à partir de mon form
 
   this.registrationForm = this.formBuilder.group({
    uid: '',
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    firstName: ['', Validators.required ],
    lastName: ['', Validators.required ],
    phone: ['', Validators.required ],
    adress: ['', Validators.required ],
    postalCode: ['', Validators.required ],
    city: ['', Validators.required ],
    roles: [],
    validation: '',
    contracts: ['', Validators.required ]
   });
 }

//  TODO: En cas d'erreur du form, le reset pour éviter les bugs 
 onSubmitRegistrationForm(){
  const email = this.registrationForm.get('email').value;
  const password = this.registrationForm.get('password').value;
  const newUser: User = this.registrationForm.value;

  newUser.photos = this.photosAdded ? this.photosAdded: [];

  this.authentication.registration(email, password).then(
    (data) => {
      console.log(data);
      console.log('UID' + data['user']['uid']);
      console.log(newUser);
      newUser.password = '';
      newUser.uid = data['user']['uid'];
      // newUser.roles = ['user'];
      newUser.roles = ['user'];
      newUser.validation = false;
 
      let contracts = [];
      console.log(newUser.contracts);
      const start = new Date(this.registrationForm.value.contracts);
      console.log(start);

      const end = new Date();
      console.log(end);

      end.setDate(start.getDate() + 365);

      console.log(start);
      console.log(end);

      const dateFormatStart = new Intl.DateTimeFormat('fr', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      }).format(start);

      const dateFormatEnd = new Intl.DateTimeFormat('fr', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      }).format(end);

      console.log(dateFormatStart);
      console.log(dateFormatEnd);

      const splitStart = dateFormatStart.split('/');
      const splitEnd = dateFormatEnd.split('/');
      const dateStart = splitStart[2] + '-' + splitStart[1] + '-' + splitStart[0];
      const dateEnd = splitEnd[2] + '-' + splitEnd[1] + '-' + splitEnd[0];

      contracts.push({
        start : dateStart,
        end : dateEnd,
      });
      console.log(contracts);
      
      newUser.contracts = contracts;

      this.usersService.createUser(newUser);

      this.router.navigate(['/']) // /admin/dashboard
    }
   ).catch(
     (error) => {
       this.errorForm = error;
       console.log(error);
     }
   );
 }

   // Gestion de l'upload de mon file 
   onUploadFile(event){
    this.photoUploading = true;
    this.usersService.uploadFile(event.target.files[0]).then(
      (url: string) => {
        this.photosAdded.push(url);
        this.photoUploading = false;
        this.photoUploaded = true;
        setTimeout(() => {
          this.photoUploaded = false;
        }, 5000)
      }
    );
  }
  
  onRemoveAddedPhoto(index){
    // this.propertiesService.removeFile(this.photosAdded[index]);
    // this.photosAdded.splice(index, 1);
  }
}

