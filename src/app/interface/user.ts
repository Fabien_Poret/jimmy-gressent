export interface User {
    uid: string,
    email: string,
    firstName: string,
    lastName: string,
    password: string,
    phone: string,
    adress: string,
    postalCode: string,
    city: string,
    validation: boolean,
    photos?: any[],
    roles: any[],
    contracts?: any[]
}
