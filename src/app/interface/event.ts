export interface Event {
    id?: string,
    title: string,
    start: string,
    end: string,
    backgroundColor?: string,
    description?: string,
    duration: number,
    users?: any[]
}
